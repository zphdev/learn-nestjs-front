import axios from 'axios';
import { Toast } from 'vant';

const axiosInstanceLoading = axios.create();

// 请求拦截
axiosInstanceLoading.interceptors.request.use(
  config => {
    const toast = Toast.loading({
      mask: true,
      duration: 0,
      // 禁用背景点击
      forbidClick: true,
      message: '加载中...',
    });

    setTimeout(() => {
      toast.message = '当前网速较慢,请耐心等待！';
    }, 3000);

    return config;
  },
  error => Promise.reject(error),
);

// 响应拦截
axiosInstanceLoading.interceptors.response.use(
  response => {
    Toast.clear('clearAll');
    const { code, message, responseBody } = response.data || {
      code: '',
      message: '',
      responseBody: '',
    };
    let result = {
      code: code,
      message: message,
    };
    if (code && code === '00') {
      result = Object.assign(result, responseBody);
    } else if (code && (code === 'EM00105' || code === 'EM00102')) {
      result = Object.assign(result, responseBody);
    } else {
      message && Toast(message);
    }
    return result;
  },
  error => {
    Toast.clear('clearAll');
    Toast('网络不给力，请检查网络设置');
    return Promise.reject(error);
  },
);

const axiosInstanceNoLoading = axios.create();

// 请求拦截
axiosInstanceNoLoading.interceptors.request.use(
  config => {
    return config;
  },
  error => Promise.reject(error),
);

// 响应拦截
axiosInstanceNoLoading.interceptors.response.use(
  response => {
    const { code, message, responseBody } = response.data || {
      code: '',
      message: '',
      responseBody: '',
    };
    let result = {
      code: code,
      message: message,
    };
    if (code && code === '00') {
      result = Object.assign(result, responseBody);
    } else if (code && (code === 'EM00105' || code === 'EM00102')) {
      result = Object.assign(result, responseBody);
    } else {
      message && Toast(message);
    }
    return result;
  },
  error => {
    Toast('网络不给力，请检查网络设置');
    return Promise.reject(error);
  },
);

/**
 * POST 请求
 * @param {string} url 接口的地址
 * @param {Object} data 请求参数
 * @returns {Promise} 返回的内容中最少包含一个 code 码
 */
const POST = (url, data = {}, isLoading = true) => {
  if (!url) return;
  if (isLoading) {
    return axiosInstanceLoading({
      url: url,
      method: 'post',
      data: data,
    });
  } else {
    return axiosInstanceNoLoading({
      url: url,
      method: 'post',
      data: data,
    });
  }
};
const GET = (url, data = {}, isLoading = true) => {
  if (!url) return;
  if (isLoading) {
    return axiosInstanceLoading({
      url: url,
      method: 'get',
      data: data,
    });
  } else {
    return axiosInstanceNoLoading({
      url: url,
      method: 'get',
      data: data,
    });
  }
};

export { POST, GET };
