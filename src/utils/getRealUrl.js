/**
 * 获取实际请求的 URL，会根据配置文件和实际打包环境请求接口
 */

const getRealUrl = ({ mockId, apiUrl }) => {
  const { VUE_APP_BASE_API, VUE_APP_ENV } = process.env;
  if (!VUE_APP_BASE_API && VUE_APP_ENV === 'development') {
    return `/${mockId}${apiUrl}`;
  }
  return `${apiUrl}`;
};

export default getRealUrl;
