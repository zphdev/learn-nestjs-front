/* eslint no-inline-comments: "off"*/

export default [
  // 首页
  {
    path: '/index',
    name: 'index',
    component: () =>
      import(/* webpackChunkName: "index" */ './views/index.vue'),
  },
];
