import getRealUrl from '@/utils/getRealUrl';

export const registApi = getRealUrl({
  mockId: '',
  apiUrl: '/serve/user/regist',
  desc: '测试POST',
});

export const userApi = getRealUrl({
  mockId: '',
  apiUrl: '/serve/user/getUserInfo',
  desc: '测试get',
});

export const rgistedUsersApi = getRealUrl({
  mockId: '',
  apiUrl: '/serve/user/registedUsers',
  desc: '从库中查询用户',
});

export const testGetApi = getRealUrl({
  mockId: '',
  apiUrl: '/serve/user/testGet',
  desc: '测试',
});

export const addUserApi = getRealUrl({
  mockId: '',
  apiUrl: '/serve/user/addUser',
  desc: '添加用户',
});
