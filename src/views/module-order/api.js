import getRealUrl from '@/utils/getRealUrl';

export const shoppingCartListApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/shoppingCart/shoppingCartList',
});

export const defaultSeletedApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/shoppingCart/defaultSeleted',
});

export const addOrSubtractToShoppingCartApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/shoppingCart/addOrSubtractToShoppingCart',
});

export const deleteGoodsFromShoppingCartApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/shoppingCart/deleteGoodsFromShoppingCart',
});

export const selectGoodsAttrsApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/goods/selectGoodsAttrs',
});

export const validCartInfoApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/shoppingCart/validCartInfo',
});

export const updateSkuIdApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/shoppingCart/updateSkuId',
});

export const pickGoodsListApi = getRealUrl({
  mockId: '104',
  apiUrl: '/serve/activity/pickGoodsList',
});
