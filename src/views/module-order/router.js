/* eslint no-inline-comments: "off"*/

export default [
  // 购物车
  {
    path: '/shopping-cart',
    name: 'ShoppingCart',
    component: () =>
      import(
        /* webpackChunkName: "ShoppingCart" */ './views/shopping-cart.vue'
      ),
  },
];
