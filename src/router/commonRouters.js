/**
 * 特殊的 router，没有对应的模块区分
 */

export default [
  {
    path: '/',
    redirect: '/index',
  },
];
