import Vue from 'vue';
import VueRouter from 'vue-router';
import commonRouters from './commonRouters';
import getAllRouters from './getAllRouters';

Vue.use(VueRouter);

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes: [...commonRouters, ...getAllRouters()],
});

export default router;
