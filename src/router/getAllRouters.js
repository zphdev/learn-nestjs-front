/**
 * 获取 router.js 文件中指定的 router
 */
const getAllRouters = () => {
  let routerList = [];
  const files = require.context('../views', true, /\.js$/);
  files.keys().map(key => {
    if (key.indexOf('router.js') > -1) {
      Array.prototype.push.apply(routerList, files(key).default);
    }
  });
  return routerList;
};

export default getAllRouters;
