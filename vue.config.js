'use strict';

const path = require('path');

const resolve = dir => path.join(__dirname, dir);

// 环境判断
const isDevl = process.env.NODE_ENV === 'development';
const isProd = process.env.NODE_ENV === 'production';
const isGray = process.env.NODE_ENV === 'gray';

// 代理的 URL
const mockUrl = 'http://192.168.9.200:39091/mock/';
const proxyUrl = process.env.VUE_APP_BASE_API || mockUrl;

module.exports = {
  // publicPath 使用相对路径的作用：防止路径变化加载静态资源失败
  publicPath: './',

  // 打包后静态资源存放的目录，这就是在 public 文件夹下不允许出现 static 文件夹的原因
  assetsDir: 'static',

  // 开发环境下，每次保存都检查代码，其余环境不做检查
  lintOnSave: isDevl,

  // 灰度环境中启用 source map，便于定位问题代码
  productionSourceMap: process.env.VUE_APP_ENV === 'gray',

  // 开发代理相关配置
  devServer: {
    // 不管是警告，还是错误全部展示在浏览器上
    overlay: {
      warnings: false,
      errors: false,
    },

    // 自动打开浏览器
    open: true,

    // 启动 GZip 压缩
    compress: true,

    // 关闭 host 检查
    disableHostCheck: true,

    // 代理相关的配置
    proxy: {
      '/': {
        target: proxyUrl,
        changeOrigin: true,
        pathRewrite: {
          '^/': '/',
        },
      },
    },
  },

  // CSS 的相关配置
  css: {
    // 向 CSS 相关的 loader 传递选项
    loaderOptions: {
      // 向 postcss-loader 传递内容
      postcss: {
        plugins: [
          require('autoprefixer')(),
          require('postcss-pxtorem')({
            // 基准单位设置为 37.5，也就是 37.5px 为 1rem
            rootValue: 37.5,
            // border 相关的 px 单位不做转换
            propList: ['*', '!border*'],
          }),
        ],
      },
    },
  },

  // 传递第三方插件
  // 需要安装两个插件 style-resources-loader vue-cli-plugin-style-resources-loader
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [resolve('src/styles/index.scss')],
    },
  },

  // Webpack 的合并配置，会通过 webpack-merge 合并到配置中
  configureWebpack: config => {
    // 开发环境中启用全量 source map
    if (isDevl) {
      config.devtool = 'source-map';
    }

    // 为文件夹取别名
    config.resolve.alias = {
      '@': resolve('src'),
    };

    // 生产环境和灰度环境中的相关配置
    if (isProd || isGray) {
      // 引入 CDN 配置
      Object.assign(config, {
        externals: {
          vue: 'Vue',
          vuex: 'Vuex',
          'vue-router': 'VueRouter',
          axios: 'axios',
        },
      });
      // 生产环境中移除 debugger 和 console
      config.optimization.minimizer[0].options.terserOptions.compress.drop_console = isProd;
      config.optimization.minimizer[0].options.terserOptions.compress.drop_debugger = isProd;
    }
  },

  // Webpack 的链式配置
  chainWebpack: config => {
    // 修改 html-webpack-plugin 插件选项
    config.plugin('html').tap(args => {
      args[0].utils = [];

      // 生产环境和灰度环境中加入不同的 js
      if (isGray || isProd) {
        args[0].utils.push('./utils/vue.runtime.min.js');
        args[0].utils.push('./utils/vue-router.min.js');
        args[0].utils.push('./utils/vuex.min.js');
        args[0].utils.push('./utils/axios.min.js');
        isGray && args[0].utils.push('./utils/vconsole.min.js');
      }

      // 格式化 HTML
      args[0].minify = {};
      // 删除属性的双引号
      args[0].minify.removeAttributeQuotes = true;
      // 清除空格，压缩 HTML
      args[0].minify.collapseWhitespace = true;
      // 不删除注释
      args[0].minify.removeComments = false;
      // 删除多余的属性
      args[0].minify.removeRedundantAttributes = true;
      // 删除script的类型属性，在h5下面script的type默认值：text/javascript
      args[0].minify.removeScriptTypeAttributes = true;
      // 删除style的类型属性
      args[0].minify.removeStyleLinkTypeAttributes = true;

      return args;
    });

    // 清除已有的loader, 如果不这样做会添加在此loader之后
    config.module
      .rule('svg')
      .uses.clear()
      .end();
    // 则匹配排除node_modules目录
    config.module
      .rule('svg')
      .exclude.add(/node_modules/)
      .end();
    // 添加svg新的loader处理
    config.module
      .rule('svg')
      .test(/\.svg$/)
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]',
      })
      .end();

    // 修改images loader 添加svg处理
    config.module
      .rule('images')
      .exclude.add(resolve('src/icons'))
      .end();
    config.module
      .rule('images')
      .test(/\.(png|jpe?g|gif|svg)(\?.*)?$/)
      .end();

    // 优化打包后的文件
    config.when(isGray || isProd, config => {
      config.optimization.splitChunks({
        chunks: 'all',
        cacheGroups: {
          // 打包初始化时依赖的第三方模块
          libs: {
            name: 'chunk-libs',
            test: /[\\/]node_modules[\\/]/,
            priority: 10,
            chunks: 'initial',
          },
          // 打包非初始化的第三方模块
          utils: {
            name: 'chunk-utils',
            test: /[\\/]node_modules[\\/]/,
            priority: 9,
          },
          // 将 vant 拆分成一个单独的 chunk，优先级设为最高（因为在程序初始化的时候必须加载这个 UI 库）
          vant: {
            name: 'chunk-vant',
            priority: 20,
            test: /[\\/]node_modules[\\/]_?vant(.*)/,
          },
          // 将 ytlife-ui 拆分成一个单独的 chunk，优先级低于 vant
          ytlifeUI: {
            name: 'chunk-ytlifeUI',
            priority: 19,
            test: /[\\/]node_modules[\\/]_?ytlife-ui(.*)/,
          },
          // 将公共的组件拆分成一个单独的 chunk
          commons: {
            name: 'chunk-commons',
            test: resolve('src/components'),
            minChunks: 3,
            priority: 5,
            reuseExistingChunk: true,
          },
        },
      });
    });
  },
};
