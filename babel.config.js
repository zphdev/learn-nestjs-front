module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  plugins: [
    [
      'import',
      {
        libraryName: 'vant',
        libraryDirectory: 'es',
        style: true,
      },
      'vant',
    ],
    [
      'import',
      {
        libraryName: 'ytlife-ui',
        libraryDirectory: 'es',
        style: true,
      },
      'ytlife-ui',
    ],
  ],
};
